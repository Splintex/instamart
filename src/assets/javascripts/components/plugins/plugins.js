import slick from 'slick-carousel';
import jquery from 'jquery';

export default function () {
  $('.js-crsl').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    adaptiveHeight: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000
  });
  
}

