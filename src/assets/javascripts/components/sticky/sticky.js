import $ from 'jquery';

export default function (opt) {
  var $win = $(window);
  var offset = 0;
  if ($(opt.el).length) {
    var left = $(opt.el).offset().left;
  }
  
  if (opt.offset) {
    offset = opt.offset;
  }
  
  init();
  
  function init() {
    $win.on("scroll", sticky);
    $win.on("resize", sticky);
  }
  
  function sticky() {
    if ($(opt.el).length) {
      if ($win.scrollTop() >= ($(opt.trigger).offset().top - offset)/2) {
        $(opt.el).addClass("is-animation-ready");
      }
      else {
        $(opt.el).removeClass('is-animation-ready');
      }
      if ($win.scrollTop() >= $(opt.trigger).offset().top - offset) {
        $(opt.el).addClass("is-sticky").css({
          left: left - $win.scrollLeft()
        });
      }
      else {
        $(opt.el).removeClass("is-sticky").css({
          left: "auto"
        });
      }
    }
    
  }
  
}