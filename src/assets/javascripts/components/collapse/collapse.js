export default class Collapse {
  constructor(el) {
    this.collapse = el;
    this.$collapse = $(this.collapse);
    this.collapseItem = this.collapse + '-item';
    this.$collapseItem = $(this.collapseItem);
    this.collapseBody = this.collapse + '-body';
    this._bindEvents();
  }

  _bindEvents() {
    this.$collapseItem.on('click', this._toggleCollapse.bind(this));
  }

  _toggleCollapse(event) {
    let $active = $(event.currentTarget);
    let $body = $active.parent().find(this.collapseBody);
    let $items = $active.parents(this.collapse).find(this.collapseItem);
    let $bodies = $active.parents(this.collapse).find(this.collapseBody);
    if (!$active.hasClass('is-active')) {
      $items.removeClass('is-active');
      $active.addClass('is-active');
      $bodies.slideUp(200);
      $body.slideDown(200);
      return false;
    }
    $active.removeClass('is-active');
    $body.slideUp(200);
    return false;

  }
}