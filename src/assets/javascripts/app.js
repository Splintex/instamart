import Toggle from './components/toggle/toggle';
import Sticky from './components/sticky/sticky';

import initPlugins from './components/plugins/plugins';


new Toggle('.js-toggle');

new Sticky({
  trigger: '.js-crsl',
  el: '.header',
  offset: 300
});

initPlugins();