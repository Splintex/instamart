var util = require('gulp-util');

var production = util.env.production || util.env.prod || false;
var destPath = 'build';
var srcPath = 'src';

var destAssets = destPath + '/assets';
var srcAssets = srcPath + '/assets';

var config = {
    env       : 'development',
    production: production,

    src: {
        root         : srcPath,
        templates    : srcPath,
        sass         : srcAssets + '/stylesheets',
        // path for sass files that will be generated automatically via some of tasks
        sassGen      : srcAssets + '/stylesheets/components/iconfont',
        js           : srcAssets + '/javascripts',
        img          : srcAssets + '/images',
        svg          : srcAssets + '/images/svg',
        icons        : srcAssets + '/icons',
        // path to png sources for sprite:png task
        iconsPng     : srcAssets + '/images/icons',
        // path to svg sources for sprite:svg task
        iconsSvg     : srcAssets + '/images/icons',
        // path to svg sources for iconfont task
        iconsFont    : srcAssets + '/images/icons',
        fonts        : srcAssets + '/fonts',
        lib          : srcAssets + '/lib'
    },
    dest: {
        root : destPath,
        html : destPath,
        css  : destAssets + '/stylesheets',
        js   : destAssets + '/javascripts',
        img  : destAssets + '/images',
        fonts: destAssets + '/fonts',
        lib  : destAssets + '/lib'
    },

    setEnv: function(env) {
        if (typeof env !== 'string') return;
        this.env = env;
        this.production = env === 'production';
        process.env.NODE_ENV = env;
    },

    logEnv: function() {
        util.log(
            'Environment:',
            util.colors.white.bgRed(' ' + process.env.NODE_ENV + ' ')
        );
    },

    errorHandler: require('./util/handle-errors')
};

config.setEnv(production ? 'production' : 'development');

module.exports = config;
