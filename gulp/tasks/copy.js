var gulp   = require('gulp');
var config = require('../config.js');


gulp.task('copy:js', function() {
    return gulp
        .src(config.src.js + '/**/*.*')
        .pipe(gulp.dest(config.dest.js));
});

gulp.task('copy:rootfiles', function() {
    return gulp
        .src(config.src.root + '/*.*')
        .pipe(gulp.dest(config.dest.root));
});

gulp.task('copy:img', function() {
  return gulp
      .src(config.src.img + '/**/*.*')
      .pipe(gulp.dest(config.dest.img));
});

gulp.task('copy:fonts', function() {
  return gulp
    .src(config.src.fonts + '/**/*.*')
    .pipe(gulp.dest(config.dest.fonts));
});

gulp.task('copy', [
    'copy:img',
    'copy:js',
    'copy:fonts'
]);
gulp.task('copy:watch', function() {
    gulp.watch(config.src.img+'/*', ['copy']);
});
