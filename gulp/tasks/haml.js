var gulp        = require('gulp');
var haml        = require('gulp-haml');
var plumber     = require('gulp-plumber');
var changed     = require('gulp-changed');
var gulpif      = require('gulp-if');
var frontMatter = require('gulp-front-matter');
var prettify    = require('gulp-prettify');
var config      = require('../config');

function renderHtml(onlyChanged) {
    return gulp
        .src(config.src.templates + '/[^_]*.haml')
        .pipe(plumber({ errorHandler: config.errorHandler }))
        .pipe(gulpif(onlyChanged, changed(config.dest.html, { extension: '.html' })))
        .pipe(frontMatter({ property: 'data' }))
        .pipe(haml().on('error', function(e) { console.log(e.message); }))
        .pipe(prettify({
            indent_size: 4,
            wrap_attributes: 'auto', // 'force'
            preserve_newlines: true,
            // unformatted: [],
            end_with_newline: true
        }))
        .pipe(gulp.dest(config.dest.html));
}

gulp.task('haml', function() {
    return renderHtml();
});

gulp.task('haml:changed', function() {
    return renderHtml(true);
});

gulp.task('haml:watch', function() {
    gulp.watch([config.src.templates + '/**/_*.haml'], ['haml']);
    gulp.watch([config.src.templates + '/**/[^_]*.haml'], ['haml:changed']);
});
