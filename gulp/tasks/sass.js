var gulp         = require('gulp');
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var mqpacker     = require('css-mqpacker');
var config       = require('../config');

var processors = [
    autoprefixer({
        browsers: ['last 4 versions'],
        cascade: false
    })
];

gulp.task('sass', function() {
    return gulp
        .src(config.src.sass + '/**/*.scss')
        //.src(config.src.sass + '/app.scss')
        .pipe(sass())
        .on('error', config.errorHandler)
        .pipe(postcss(processors))
        .pipe(gulp.dest(config.dest.css));
});

gulp.task('sass:watch', function() {
    gulp.watch(config.src.sass + '/**/*.scss', ['sass']);
});
