(function() {
  const fakeSelect = '.js-select';
  const $select = $('.js-select select');
  const head = '.js-select-head';
  function bindEvents() {
    $select.on('change', changeValue);
  }
  function changeValue() {
    $(this).parents(fakeSelect).find(head).text($(this).find("option:selected").text());
  }
  bindEvents();
})();