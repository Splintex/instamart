export default class Toggle {
  constructor(el) {
    this.btn = el;
    this.$btn = $(this.btn);
    this.$root = $('html');
    this._bindEvents();
  }
  _bindEvents() {
    this.$btn.on('click', this._clickAction.bind(this));
  }
  _clickAction(event) {
    const $active = $(event.currentTarget);
    const targetClass = $active.data('toggle');
    const rootClass = $active.data('class');
    const anim = $active.data('anim');
    if ($active.hasClass('is-active')) {
      $(targetClass).removeClass('is-active');
      $('[data-toggle="' + targetClass + '"]').removeClass('is-active');
    }
    else {
      $(targetClass).addClass('is-active');
      $('[data-toggle="' + targetClass + '"]').addClass('is-active');
    }

    if (rootClass) {
      this.$root.toggleClass(rootClass);
    }
    if (anim == "slide") {
      $(targetClass).slideToggle(200);
    }
    if (anim == "fade") {
      $(targetClass).toggle(200);
    }
    return false;
  }
}
