import $ from 'jquery';

export default class {
  constructor(el) {
    this.tabs = el;
    this.tabLink = this.tabs + ' a';
    this.bindEvents();
  }
  bindEvents() {
    $(this.tabLink).on('click', this.changeTab.bind(this));
  }
  changeTab(event) {
    const $tab = $(event.currentTarget);
    const $tabs = $tab.closest(this.tabs);
    const $group = $('[data-tabs="' + $tabs.data('content') + '"]');
    const $content = $('[data-id="' + $tab.attr('href') + '"]');
    $tabs.find('li').removeClass('is-active');
    $tab.parent().addClass('is-active');
    $group.hide();
    $content.show();
    return false;
  }
}
